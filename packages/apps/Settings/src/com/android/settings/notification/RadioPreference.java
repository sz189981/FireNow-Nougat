package com.android.settings.notification;

import com.android.settings.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceGroup;
import android.text.TextUtils;
import android.util.AttributeSet;

public class RadioPreference extends CheckBoxPreference {
    private String mRadioGroup;

    public RadioPreference(Context context) {
        this(context, null);
    }

    public RadioPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        final TypedArray a =
                context.obtainStyledAttributes(attrs, R.styleable.RadioPreference, 0, 0);

        mRadioGroup = a.getString(R.styleable.RadioPreference_radioGroup);

        a.recycle();

        setWidgetLayoutResource(R.layout.radio_preference_widget);
    }

    public String getRadioGroup() {
        return mRadioGroup;
    }

    public void setRadioGroup(String radioGroup) {
        mRadioGroup = radioGroup;
    }

    public void clearOtherRadioPreferences(PreferenceGroup preferenceGroup) {
        final int count = preferenceGroup.getPreferenceCount();
        for (int i = 0; i < count; i++) {
            final Preference p = preferenceGroup.getPreference(i);
            if (!(p instanceof RadioPreference)) {
                continue;
            }
            final RadioPreference radioPreference = (RadioPreference) p;
            if (!TextUtils.equals(getRadioGroup(), radioPreference.getRadioGroup())) {
                continue;
            }
            if (TextUtils.equals(getKey(), radioPreference.getKey())) {
                continue;
            }
            radioPreference.setChecked(false);
        }
    }
}
